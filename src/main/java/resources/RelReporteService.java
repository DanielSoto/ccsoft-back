package resources;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.relreporteDAO;
import entity.RelReporte;
import entity.Result;

@Path("/RelReporteService")
public class RelReporteService {
	@Context
    private ServletContext context;
	relreporteDAO dao=new relreporteDAO();
	
	Result result;
	Response response= null;
	
	/**
	* Obtiene todas las relaciones
	* <strong>Type:</strong> GET <br>
	* <strong>URL:</strong> /cssoft/API/RelReporteService/... <br>
	* @author Aryam Ruiz
	*/
	@GET
	@Path("/getRelReporte")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRelReporte(){
		Response response = null;
		Result result;
		result = dao.getRelReporte();
		if(result.getError().equals("false")) {
			response = Response.ok(result).build();
		}else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}
	
	/**
	 * Inserta una relacin de tipo Reporte-Usuario
	 * @autor Arturo Acua
	 * @param entity RelReporte
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/insertRelReporte")
	public Response insertRelReporte(RelReporte relrep) {
		Response response = null;
		result = dao.insertRelReporte(relrep);
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}

}
