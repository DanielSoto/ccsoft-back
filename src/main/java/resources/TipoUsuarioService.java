package resources;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.registrousuariosDAO;
import entity.Correo;
import entity.CorreoSoporte;
import entity.RegistroUsuarios;
import entity.Result;

@Path("/RegistroUsuarios")

public class TipoUsuarioService {
	
	@Context
    private ServletContext context;
	registrousuariosDAO dao=new registrousuariosDAO();
	
	Result result;
	Response response= null;
	
	/**
	* Obtiene todos los registros de Registro Usuario
	* <strong>Type:</strong> GET <br>
	* <strong>URL:</strong> /cssoft/API/TipoUsuarios/... <br>
	* @author Aryam Ruiz
	*/
	@GET
	@Path("/getRegistroUsuarios")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRegistroUsuarios(){
		Response response = null;
		Result result;
		result = dao.getRegistroUsuarios();
		if(result.getError().equals("false")) {
			response = Response.ok(result).build();
		}else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}
	
	/**
	    * @author Aryam Ruiz
	    * @param entity RegistroUsuario
	    * @return
	    */
	    @POST
		@Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
	    @Path("/insertDatosRegistro")
	    public Response insertDatosRegistro(RegistroUsuarios registroUsuarios) {
	        Response response = null;
	        Result result;
	        result = dao.insertDatosRegistro(registroUsuarios);
	        if(result.getError().equals("false")) {
	            response = Response.ok(result).build();
	        }else {
	            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
	        }
	        return response;
	        
	    }
	    
		/**
		* Obtiene todos los registros de Registro Usuario por el username
		* <strong>Type:</strong> GET <br>
		* <strong>URL:</strong> /cssoft/API/TipoUsuarios/... <br>
		* @author Aryam Ruiz
		*/
		@GET
		@Path("/getRegistroUsuariosPorUsername/{usuario}")
		@Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
		public Response getRegistroUsuariosPorUsername(@PathParam("usuario") String usuario){
			Response response = null;
			Result result;
			result = dao.getRegistroUsuariosPorUsername(usuario);
			if(result.getError().equals("false")) {
				response = Response.ok(result).build();
			}else {
				response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
			}
			return response;
		}
		
		/**
		* Obtiene todos los registros de Registro Usuario por el username y contraseņa
		* <strong>Type:</strong> GET <br>
		* <strong>URL:</strong> /cssoft/API/TipoUsuarios/... <br>
		* @author Aryam Ruiz
		*/
		@GET
		@Path("/getUserFromLogin/{usuario}/{contrasena}")
		@Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
		public Response getUserFromLogin(@PathParam("usuario") String usuario, @PathParam("contrasena") String contrasena){
			Response response = null;
			Result result;
			result = dao.getUserFromLogin(usuario, contrasena);
			if(result.getError().equals("false")) {
				response = Response.ok(result).build();
			}else {
				response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
			}
			return response;
		}

		/**
		* Envia un correo
		* <strong>Type:</strong> GET <br>
		* <strong>URL:</strong> /cssoft/API/TipoUsuarios/... <br>
		* @author Aryam Ruiz
		*/
		@POST
		@Path("/SendMail")
		@Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
		public Response SendMail(Correo correo){
			Response response = null;
			Result result;
			result = dao.SendMail(correo);
			if(result.getError().equals("false")) {
				response = Response.ok(result).build();
			}else {
				response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
			}
			return response;
		}
		
		/**
		* Obtiene todos los registros de Registro Usuario por el correo
		* <strong>Type:</strong> GET <br>
		* <strong>URL:</strong> /cssoft/API/TipoUsuarios/... <br>
		* @author Aryam Ruiz
		*/
		@GET
		@Path("/getRegistroUsuariosPorCorreo/{correo}")
		@Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
		public Response getRegistroUsuariosPorCorreo(@PathParam("correo") String correo){
			Response response = null;
			Result result;
			result = dao.getRegistroUsuariosPorCorreo(correo);
			if(result.getError().equals("false")) {
				response = Response.ok(result).build();
			}else {
				response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
			}
			return response;
		}
		
		/**
		* Obtiene todos los registros de Registro Usuario por la clave
		* <strong>Type:</strong> GET <br>
		* <strong>URL:</strong> /cssoft/API/TipoUsuarios/... <br>
		* @author Aryam Ruiz
		*/
		@GET
		@Path("/getRegistroUsuariosPorClave/{clave}")
		@Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
		public Response getRegistroUsuariosPorClave(@PathParam("clave") String clave){
			Response response = null;
			Result result;
			result = dao.getRegistroUsuariosPorClave(clave);
			if(result.getError().equals("false")) {
				response = Response.ok(result).build();
			}else {
				response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
			}
			return response;
		}
		
		/**
		* Actualiza la contraseņa
		* <strong>Type:</strong> POST <br>
		* <strong>URL:</strong> /cssoft/API/TipoUsuarios/... <br>
		* @author Aryam Ruiz
		*/
		@POST
		@Path("/updateContrasena")
		@Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
		public Response updateContrasena(RegistroUsuarios registroUsuarios){
			Response response = null;
			Result result;
			result = dao.updateContrasena(registroUsuarios);
			if(result.getError().equals("false")) {
				response = Response.ok(result).build();
			}else {
				response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
			}
			return response;
		}
		
		/**
		* Actualiza la info del perfil
		* <strong>Type:</strong> POST <br>
		* <strong>URL:</strong> /cssoft/API/TipoUsuarios/... <br>
		* @author Aryam Ruiz
		*/
		@POST
		@Path("/updatePerfil")
		@Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
		public Response updatePerfil(RegistroUsuarios registroUsuarios){
			Response response = null;
			Result result;
			result = dao.updatePerfil(registroUsuarios);
			if(result.getError().equals("false")) {
				response = Response.ok(result).build();
			}else {
				response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
			}
			return response;
		}
		
		/**
		* Envia un correo a soporte
		* <strong>Type:</strong> POST <br>
		* <strong>URL:</strong> /cssoft/API/TipoUsuarios/... <br>
		* @author Aryam Ruiz
		*/
		/*@POST
		@Path("/SendMailSoporte")
		@Produces(MediaType.APPLICATION_JSON)
	    @Consumes(MediaType.APPLICATION_JSON)
		public Response SendMailSoporte(CorreoSoporte correosoporte){
			Response response = null;
			Result result;
			result = dao.SendMailSoporte(correosoporte);
			if(result.getError().equals("false")) {
				response = Response.ok(result).build();
			}else {
				response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
			}
			return response;
		}*/
}
