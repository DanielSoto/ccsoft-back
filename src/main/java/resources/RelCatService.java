package resources;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.relcatDAO;
import entity.Result;
import entity.RelCat;

@Path("/RelCatService")
public class RelCatService {
	@Context
    private ServletContext context;
	relcatDAO dao=new relcatDAO();
	
	Result result;
	Response response= null;
	
	/**
	* Obtiene todas las relaciones
	* <strong>Type:</strong> GET <br>
	* <strong>URL:</strong> /cssoft/API/RelCatService/... <br>
	* @author Aryam Ruiz
	*/
	@GET
	@Path("/getRelCat")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRelCat(){
		Response response = null;
		Result result;
		result = dao.getRelCat();
		if(result.getError().equals("false")) {
			response = Response.ok(result).build();
		}else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}
	
	/**
	 * Inserta una relacin de tipo Reporte-Categora
	 * @autor Arturo Acua
	 * @param entity RelCat
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/insertRelCat")
	public Response insertRelCat(RelCat relcat) {
		Response response = null;
		result = dao.insertRelCat(relcat);
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}
}
