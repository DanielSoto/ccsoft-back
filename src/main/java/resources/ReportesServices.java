package resources;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.reportesDAO;
import entity.Reportes;
import entity.Result;

@Path("/ReportesService")
public class ReportesServices {

	@Context
	private ServletContext context;
	reportesDAO dao = new reportesDAO();

	Result result;
	Response response = null;

	/**
	 * Obtiene todos los registros de Reportes <strong>Type:</strong> GET <br>
	 * <strong>URL:</strong> /cssoft/API/CategoriasService/... <br>
	 * 
	 * @author Aryam Ruiz
	 */
	@GET
	@Path("/getReportes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReportes() {
		Response response = null;
		Result result;
		result = dao.getReportes();
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}

	/**
	 * @author Aryam Ruiz
	 * @param entity Reportes
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/insertReportes")
	public Response insertReportes(Reportes reportes) {
		Response response = null;
		Result result;
		result = dao.insertReportes(reportes);
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;

	}
	
	/**
	 * @author Arturo Acu�a
	 * @param entity Reportes
	 * @return
	 */
	@GET
	@Path("/updateReportesEstatus/{estatus}/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateReportesEstatus(@PathParam("estatus") String estatus, @PathParam("id") Integer id) {
		Response response = null;
		Result result;
		result = dao.updateReportesEstatus(estatus, id);
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;

	}
	
	/**
	 * @author Arturo Acu�a
	 * @param entity Reportes
	 * @return
	 */
	@GET
	@Path("/getReportesDenunciasAdmin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getReportesDenunciasAdmin() {
		Response response = null;
		Result result;
		result = dao.getReportesDenunciasAdmin();
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}
	
	/**
	 * @author Arturo Acu�a
	 * @param entity Reportes
	 * @return
	 */
	@GET
	@Path("/getReportesDenuncias/{usuario}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getReportesDenuncias(@PathParam("usuario") String usuario) {
		Response response = null;
		Result result;
		result = dao.getReportesDenuncias(usuario);
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}
	/**
	 * Retorna el ID del �ltimo registro - Esto para registrar la imagen con el ID del reporte.
	 * 
	 * @author Arturo Acu�a
	 */
	@GET
	@Path("/getReporteLastID")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReportesLastID() {
		Response response = null;
		Result result;
		result = dao.getReporteLastID();
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}
}
