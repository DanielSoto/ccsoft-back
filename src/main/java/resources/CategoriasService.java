package resources;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.categoriasDAO;
import entity.Categorias;
import entity.Result;

@Path("/CategoriasService")
public class CategoriasService {

	@Context
	private ServletContext context;
	categoriasDAO dao = new categoriasDAO();

	Result result;
	Response response = null;

	/**
	 * Obtiene todos los registros de categorias <strong>Type:</strong> GET <br>
	 * <strong>URL:</strong> /cssoft/API/CategoriasService/... <br>
	 * 
	 * @author Aryam Ruiz
	 */
	@GET
	@Path("/getCategorias")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCategorias() {
		Response response = null;
		Result result;
		result = dao.getCategorias();
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}

	/**
	 * @author Aryam Ruiz
	 * @param entity Categorias
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/insertCategorias")
	public Response insertCategorias(Categorias categorias) {
		Response response = null;
		Result result;
		result = dao.insertCategorias(categorias);
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;

	}

}
