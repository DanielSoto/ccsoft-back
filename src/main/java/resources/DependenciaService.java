package resources;

import javax.servlet.ServletContext;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import dao.dependenciaDAO;
import entity.Dependencia;
import entity.Result;

@Path("/DependenciaService")
public class DependenciaService {

	@Context
	private ServletContext context;
	dependenciaDAO dao = new dependenciaDAO();
	
	Result result;
	Response response = null;

	/**
	 * @author Arturo Acuna
	 * @param entity Dependencia
	 * @return
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/insertDependencia")
	public Response insertReportes(Dependencia dependencia) {
		Response response = null;
		Result result;
		result = dao.insertDependencia(dependencia);
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;

	}
	
	/**
	 * Obtiene todos los registros de Dependencias <strong>Type:</strong> GET <br>
	 * <strong>URL:</strong> /cssoft/API/DependenciaService/... <br>
	 * 
	 * @author Arturo Acuna
	 */
	@GET
	@Path("/getDependencias")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDependencias() {
		Response response = null;
		Result result;
		result = dao.getDependencias();
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}
	
	/**
	 * @author Arturo Acu�a
	 * @param entity Dependencia
	 * @return
	 */
	@GET
	@Path("/getDependenciaReporte/{id_reporte}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getReportesDenuncias(@PathParam("id_reporte") String id_reporte) {
		Response response = null;
		Result result;
		result = dao.getDependenciaReporte(id_reporte);
		if (result.getError().equals("false")) {
			response = Response.ok(result).build();
		} else {
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		return response;
	}
}
