package db;

import java.sql.Connection;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
	
public class DBConnect {

	public final Connection getConnection() throws Exception
		{
			Context initContext = new InitialContext();
			DataSource dataSource = (DataSource) initContext.lookup("java:jboss/datasources/ccsoft");
		      return dataSource.getConnection();
		}
}
