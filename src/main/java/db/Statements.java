package db;

public class Statements {
	//GET	
	public static final String GET_CATEGORIAS="SELECT id_categorias, nombre FROM ccsoft.`categorķas`";
	
	public static final String GET_TIPO_USUARIOS = "SELECT id_registro, nombres, apellidos, correo, usuario, contrasena, sexo, tipoUsuario FROM ccsoft.registrousuarios";
	
	public static final String GET_RELCAT = "SELECT idCat, idReporte FROM ccsoft.`relcat`";
	
	public static final String INSERT_RELCAT = "INSERT INTO ccsoft.`relcat` (idCat, idReporte) VALUES ((SELECT id_categorias from ccsoft.`categorķas` where nombre = ?),(SELECT id_reportes FROM ccsoft.`reportes` ORDER BY id_reportes DESC LIMIT 0, 1));";
	
	public static final String GET_RELREPORTE = "SELECT idReporte, idUsuario FROM ccsoft.`relreporte`";
	
	public static final String GET_REPORTES = "SELECT id_reportes, folio, nombre, telefono, telefonoAlt, nombreDenunciado, calle, entreCalles, colonia, cp, estatus, descripcion, correo, imgUrl, fecha FROM ccsoft.`reportes`";
	
	public static final String INSERT_REGISTRO_USUARIO = "INSERT INTO ccsoft.`registrousuarios` (nombres, apellidos, correo, sexo, usuario, contrasena, tipoUsuario) VALUES (?,?,?,?,?,?,?)";
	
	public static final String INSERT_CATEGORIAS = "INSERT INTO ccsoft.`categorķas` (nombre) VALUES (?)";
	
	public static final String INSERT_REPORTES = "INSERT INTO ccsoft.`reportes` (folio, nombre, telefono, telefonoAlt, nombreDenunciado, calle, entreCalles, colonia, cp, estatus, descripcion, correo, imgUrl, fecha) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	public static final String INSERT_RELREPORTE = "INSERT INTO relreporte (idUsuario,idReporte)VALUES((Select id_registro from registrousuarios where usuario = ?),(SELECT id_reportes FROM ccsoft.`reportes` ORDER BY id_reportes DESC LIMIT 0, 1))";
	
	public static final String GET_TIPO_USUARIOS_POR_USERNAME = "SELECT id_registro, nombres, apellidos, correo, usuario, contrasena, sexo, tipoUsuario FROM ccsoft.registrousuarios WHERE usuario = ?";

	public static final String GET_TIPO_USUARIOS_POR_CORREO = "SELECT id_registro, nombres, apellidos, correo, usuario, contrasena, sexo, tipoUsuario FROM ccsoft.registrousuarios WHERE correo = ?";

	public static final String GET_USUARIO_LOGIN = "SELECT id_registro, nombres, apellidos, correo, sexo, usuario, contrasena, tipoUsuario FROM registrousuarios WHERE BINARY usuario LIKE ? and BINARY contrasena LIKE ?";
	
	public static final String UPDATE_CODIGO_REGISTRO = "UPDATE registrousuarios SET clave = ? WHERE correo = ?";
	
	public static final String GET_TIPO_USUARIOS_POR_CLAVE = "SELECT id_registro, nombres, apellidos, correo, usuario, contrasena, sexo, tipoUsuario, clave FROM ccsoft.registrousuarios WHERE clave = ?";
	
	public static final String UPDATE_CONTRASENA = "UPDATE registrousuarios SET contrasena = ? WHERE clave = ?";
		
	public static final String UPDATE_PERFIL = "UPDATE registrousuarios SET usuario = ?, contrasena = ? WHERE id_registro = ?";

	public static final String GET_REPORTES_DENUNCIAS = "SELECT r.id_reportes, r.folio,r.nombre,r.descripcion,c.nombre AS categoria,r.fecha,r.estatus FROM reportes r INNER JOIN relcat rc ON r.id_reportes = rc.idReporte INNER JOIN categorķas c ON rc.idCat = c.id_categorias INNER JOIN relreporte rp on r.id_reportes = rp.idReporte inner join registrousuarios r2 on rp.idUsuario = r2.id_registro where r2.usuario = ?";
	
	public static final String GET_REPORTES_DENUNCIAS_ADMIN = "SELECT r.id_reportes, r.folio,r.nombre,r.descripcion,c.nombre AS categoria,r.fecha,r.estatus FROM reportes r INNER JOIN relcat rc ON r.id_reportes = rc.idReporte INNER JOIN categorķas c ON rc.idCat = c.id_categorias order by id_reportes ASC";
	
	public static final String UPDATE_REPORTES_ESTATUS = "UPDATE reportes set estatus = ? where id_reportes = ?";
	
	public static final String GET_REPORTE_LAST_ID = "SELECT id_reportes FROM ccsoft.`reportes` ORDER BY id_reportes DESC LIMIT 0, 1";
	
	public static final String GET_REPORTE_ALL_ID = "SELECT * from reportes where id_reportes = ?";
	
	public static final String GET_DEPENDENCIA_ALL_ID = "SELECT * from dependencias d inner join reldeprep dr on d.id_dependencia = dr.id_dependencia where dr.id_reporte = ?";
	
	public static final String INSERT_RELDEPCAT = "INSERT into reldepcat(id_dependencia, id_categoria) VALUES (?,?)";
			
	public static final String INSERT_RELDEPREP = "INSERT into reldeprep(id_dependencia, id_reporte) VALUES (?,?)"; 
	
	public static final String GET_RELDEPREP = "SELECT * FROM reldeprep";

	public static final String INSERT_DEPENDENCIA = "INSERT into dependencias (nombre, municipio, calle, entreCalles, responsable, correo, telefono) values (?,?,?,?,?,?,?)";
	
	public static final String GET_RELDEPCAT = "SELECT * FROM reldepcat";
	
	public static final String GET_DEPENDENCIAS = "SELECT * FROM dependencias";

	//public static final String INSERT_RELDEPCAT = "INSERT into reldepcat(id_dependencia, id_categoria) VALUES (Select id_dependencia from dependencias where nombre = ?,Select id_categorias from categorķas where nombre = ?)";
}
