
package security;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class CrossOriginResourceSharingFilter implements ContainerResponseFilter {

	
	public void filter(ContainerRequestContext creqContext, ContainerResponseContext creqResponse) throws IOException {
		creqResponse.getHeaders().add("Access-Control-Allow-Origin", "*");
		creqResponse.getHeaders().add("Access-Control-Allow-Credentials", "true");
		creqResponse.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		creqResponse.getHeaders().add("Access-Control-Expose-Headers","Content-Range");
		creqResponse.getHeaders().add("Access-Control-Allow-Headers", 
				"Content-Type, Accept, Cache-Control, X-Requested-With, Authorization, Data, Condition");
	}


}
