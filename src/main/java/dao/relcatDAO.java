package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import db.DBConnect;
import db.Statements;
import entity.RelCat;
import entity.Result;

public class relcatDAO {

	// /*Se utiliza como loggeador de problemas para detectar fallos en ejecuciones
	// de codigo*/
	private static Logger LOGGER = Logger.getLogger(relcatDAO.class);
	Connection con;
	Statement stm;
	ResultSet rs;
	PreparedStatement pstm;
	Integer metodo = 0;

	private void closeObj(Connection con, Statement stm, ResultSet rs, PreparedStatement pstm) {
		try {
			if (null != rs) {
				rs.close();
			}
			if (null != stm) {
				stm.close();
			}
			if (null != con) {
				con.close();
			}
			if (null != pstm) {
				pstm.close();
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage() + " - " + e.getErrorCode());
		}
	}

	/**
	 * Obtiene los registros de RelCat
	 * 
	 * @author Aryam Ruiz
	 * @param
	 */
	public Result getRelCat() {
		Result result = new Result();
		List<RelCat> lstResultado = new ArrayList<RelCat>();
		RelCat objRelCat = new RelCat();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_RELCAT);
			rs = pstm.executeQuery();

			while (rs.next()) {
				objRelCat = new RelCat();
				objRelCat.setIdCat(rs.getInt("idCat"));
				objRelCat.setIdReporte(rs.getInt("idReporte"));

				lstResultado.add(objRelCat);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Inserta un registro de RelCat
	 * 
	 * @author Arturo Acua
	 * @param
	 */
	public final Result insertRelCat(RelCat relcat) {
		Result result = new Result();
		List<RelCat> lstResultado = new ArrayList<RelCat>();
		
		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();
			con.setAutoCommit(false);
			
			pstm = con.prepareStatement(Statements.INSERT_RELCAT);
			
			pstm.setString(1, relcat.getNombre());		
			pstm.executeUpdate();
			
			con.commit();
			con.setAutoCommit(true);
			
			result.setSuccess(true);
			result.setError("false");
			result.setData(lstResultado);
		} catch (Exception e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e.getMessage());
			result.setError(e.getMessage());
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}

	
}
