package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import db.DBConnect;
import db.Statements;
import entity.Dependencia;
import entity.Result;

public class dependenciaDAO {
	// /*Se utiliza como loggeador de problemas para detectar fallos en ejecuciones de codigo*/
	private static Logger LOGGER = Logger.getLogger(relcatDAO.class);
	Connection con;
	Statement stm;
	ResultSet rs;
	PreparedStatement pstm;
	Integer metodo = 0;

	private void closeObj(Connection con, Statement stm, ResultSet rs, PreparedStatement pstm) {
		try {
			if (null != rs) {
				rs.close();
			}
			if (null != stm) {
				stm.close();
			}
			if (null != con) {
				con.close();
			}
			if (null != pstm) {
				pstm.close();
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage() + " - " + e.getErrorCode());
		}
	}
	
	/**
	 * Inserta los datos de la dependencia
	 * @author Arturo Acuna
	 * @param
	 */
	public final Result insertDependencia(Dependencia dependencia) {
		
		Result result = new Result();
		List<Dependencia> lstResultado = new ArrayList<Dependencia>();

		try {
			
			DBConnect db = new DBConnect();
			con = db.getConnection();
			con.setAutoCommit(false);
			
			pstm = con.prepareStatement(Statements.INSERT_DEPENDENCIA);

			pstm.setString(1, dependencia.getNombre());
			pstm.setString(2, dependencia.getMunicipio());
			pstm.setString(3, dependencia.getCalle());
			pstm.setString(4, dependencia.getEntre_Calles());
			pstm.setString(5, dependencia.getResponsable());
			pstm.setString(6, dependencia.getCorreo());
			pstm.setString(7, dependencia.getTelefono());
			pstm.executeUpdate();
			
			con.commit();
			con.setAutoCommit(true);

			result.setSuccess(true);
			result.setError("false");
			result.setData(lstResultado);

		} catch (Exception e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e.getMessage());
			result.setError(e.getMessage());
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Obtiene los registros de Dependencia
	 * @author Arturo Acuna
	 * @param
	 */
	public Result getDependencias() {
		Result result = new Result();
		List<Dependencia> lstResultado = new ArrayList<Dependencia>();
		Dependencia objDependencia = new Dependencia();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();
			pstm = con.prepareStatement(Statements.GET_DEPENDENCIAS);
			rs = pstm.executeQuery();
			while (rs.next()) {
				objDependencia = new Dependencia();
				objDependencia.setId_dependencia(rs.getInt("id_dependencia"));
				objDependencia.setNombre(rs.getString("nombre"));
				objDependencia.setMunicipio(rs.getString("municipio"));
				objDependencia.setCalle(rs.getString("calle"));
				objDependencia.setEntre_Calles(rs.getString("entreCalles"));
				objDependencia.setResponsable(rs.getString("responsable"));
				objDependencia.setCorreo(rs.getString("correo"));
				objDependencia.setTelefono(rs.getString("telefono"));
				lstResultado.add(objDependencia);
			}
			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Obtiene los registros de Dependencia para "Mis denuncias"
	 * @author Arturo Acu�a
	 * @param
	 */
	public Result getDependenciaReporte(String id_reporte) {
		Result result = new Result();
		List<Dependencia> lstResultado = new ArrayList<Dependencia>();
		Dependencia objDependencia = new Dependencia();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();
			pstm = con.prepareStatement(Statements.GET_DEPENDENCIA_ALL_ID);
			pstm.setString(1, id_reporte);
			rs = pstm.executeQuery();
			while (rs.next()) {
				objDependencia = new Dependencia();
				objDependencia.setId_dependencia(rs.getInt("id_dependencia"));
				objDependencia.setNombre(rs.getString("nombre"));
				objDependencia.setMunicipio(rs.getString("municipio"));
				objDependencia.setCalle(rs.getString("calle"));
				objDependencia.setEntre_Calles(rs.getString("entreCalles"));
				objDependencia.setResponsable(rs.getString("responsable"));
				objDependencia.setCorreo(rs.getString("correo"));
				objDependencia.setTelefono(rs.getString("telefono"));

				lstResultado.add(objDependencia);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
}
