package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.PasswordAuthentication;

import org.apache.log4j.Logger;


import db.DBConnect;
import db.Statements;
import entity.Correo;
import entity.CorreoSoporte;
import entity.RegistroUsuarios;
import entity.Result;

public class registrousuariosDAO {

	// /*Se utiliza como loggeador de problemas para detectar fallos en ejecuciones
	// de codigo*/
	private static Logger LOGGER = Logger.getLogger(registrousuariosDAO.class);
	Connection con;
	Statement stm;
	ResultSet rs;
	PreparedStatement pstm;
	Integer metodo = 0;

	private void closeObj(Connection con, Statement stm, ResultSet rs, PreparedStatement pstm) {
		try {
			if (null != rs) {
				rs.close();
			}
			if (null != stm) {
				stm.close();
			}
			if (null != con) {
				con.close();
			}
			if (null != pstm) {
				pstm.close();
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage() + " - " + e.getErrorCode());
		}
	}

	/**
	 * Obtiene los registros de RegistroUsuarios
	 * @author Aryam Ruiz
	 * @param
	 */
	public Result getRegistroUsuarios() {
		Result result = new Result();
		List<RegistroUsuarios> lstResultado = new ArrayList<RegistroUsuarios>();
		RegistroUsuarios objRegistroUsuario = new RegistroUsuarios();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_TIPO_USUARIOS);
			rs = pstm.executeQuery();

			while (rs.next()) {
				objRegistroUsuario = new RegistroUsuarios();
				objRegistroUsuario.setId_registro(rs.getInt("id_registro"));
				objRegistroUsuario.setNombres(rs.getString("nombres"));
				objRegistroUsuario.setApellidos(rs.getString("apellidos"));
				objRegistroUsuario.setCorreo(rs.getString("correo"));
				objRegistroUsuario.setUsuario(rs.getString("usuario"));
				objRegistroUsuario.setContrasena(rs.getString("contrasena"));
				objRegistroUsuario.setSexo(rs.getString("sexo"));
				objRegistroUsuario.setTipoUsuario(rs.getString("tipoUsuario"));

				lstResultado.add(objRegistroUsuario);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}

	/**
	 * Inserta los datos del registro
	 * @author Aryam Ruiz
	 */
	public final Result insertDatosRegistro(RegistroUsuarios registroUsuarios) {
		Result result = new Result();
		List<RegistroUsuarios> lstResultado = new ArrayList<RegistroUsuarios>();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();
			con.setAutoCommit(false);
//			long idRegistro = 0;
			//String sexo = "";
			String tipoUsuario = "C"; //Como nosotros realizaremos el cambio de usuario el valor es estatico.

			pstm = con.prepareStatement(Statements.INSERT_REGISTRO_USUARIO);
			//, Statement.RETURN_GENERATED_KEYS
			pstm.setString(1, registroUsuarios.getNombres());
			pstm.setString(2, registroUsuarios.getApellidos());
			pstm.setString(3, registroUsuarios.getCorreo());
			pstm.setString(4, registroUsuarios.getSexo());
			pstm.setString(5, registroUsuarios.getUsuario());
			pstm.setString(6, registroUsuarios.getContrasena());
			pstm.setString(7, tipoUsuario);
			pstm.executeUpdate();
//			rs = pstm.getGeneratedKeys();
//			lstResultado.add(registroUsuarios);
//			if (rs.next()) {
//				idRegistro = rs.getInt(1);
//				System.out.println(idRegistro);
//			}

//			pstm = con.prepareStatement(Statements.INSERT_RELREPORTE);
//			pstm.setLong(1, 23);
//			pstm.setLong(2, idRegistro);
//
//			pstm.executeUpdate();

			con.commit();
			con.setAutoCommit(true);

			result.setSuccess(true);
			result.setError("false");
			result.setData(lstResultado);

		} catch (Exception e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e.getMessage());
			result.setError(e.getMessage());
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Obtiene los registros de RegistroUsuarios por nombre de usuario.
	 * @author Aryam Ruiz
	 * @param username
	 */
	public Result getRegistroUsuariosPorUsername(String usuario) {
		Result result = new Result();
		List<RegistroUsuarios> lstResultado = new ArrayList<RegistroUsuarios>();
		RegistroUsuarios objRegistroUsuario = new RegistroUsuarios();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_TIPO_USUARIOS_POR_USERNAME);
			pstm.setString(1, usuario);
			rs = pstm.executeQuery();

			while (rs.next()) {
				objRegistroUsuario = new RegistroUsuarios();
				objRegistroUsuario.setId_registro(rs.getInt("id_registro"));
				objRegistroUsuario.setNombres(rs.getString("nombres"));
				objRegistroUsuario.setApellidos(rs.getString("apellidos"));
				objRegistroUsuario.setCorreo(rs.getString("correo"));
				objRegistroUsuario.setUsuario(rs.getString("usuario"));
				objRegistroUsuario.setContrasena(rs.getString("contrasena"));
				objRegistroUsuario.setSexo(rs.getString("sexo"));
				objRegistroUsuario.setTipoUsuario(rs.getString("tipoUsuario"));

				lstResultado.add(objRegistroUsuario);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Obtiene los registros de RegistroUsuarios por nombre de usuario.
	 * @author Aryam Ruiz
	 * @param username
	 */
	public Result getRegistroUsuariosPorCorreo(String correo) {
		Result result = new Result();
		List<RegistroUsuarios> lstResultado = new ArrayList<RegistroUsuarios>();
		RegistroUsuarios objRegistroUsuario = new RegistroUsuarios();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_TIPO_USUARIOS_POR_CORREO);
			pstm.setString(1, correo);
			rs = pstm.executeQuery();

			while (rs.next()) {
				objRegistroUsuario = new RegistroUsuarios();
				objRegistroUsuario.setId_registro(rs.getInt("id_registro"));
				objRegistroUsuario.setNombres(rs.getString("nombres"));
				objRegistroUsuario.setApellidos(rs.getString("apellidos"));
				objRegistroUsuario.setCorreo(rs.getString("correo"));
				objRegistroUsuario.setUsuario(rs.getString("usuario"));
				objRegistroUsuario.setContrasena(rs.getString("contrasena"));
				objRegistroUsuario.setSexo(rs.getString("sexo"));
				objRegistroUsuario.setTipoUsuario(rs.getString("tipoUsuario"));

				lstResultado.add(objRegistroUsuario);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Obtiene el usuario u contrase�a para accesar.
	 * @author Aryam Ruiz
	 * @param username, contrasena
	 */
	public Result getUserFromLogin(String usuario, String contrasena) {
		Result result = new Result();
		List<RegistroUsuarios> lstResultado = new ArrayList<RegistroUsuarios>();
		RegistroUsuarios objRegistroUsuario = new RegistroUsuarios();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_USUARIO_LOGIN);
			pstm.setString(1, usuario);
			pstm.setString(2, contrasena);
			rs = pstm.executeQuery();
			
			while (rs.next()) {
				objRegistroUsuario = new RegistroUsuarios();
				objRegistroUsuario.setId_registro(rs.getInt("id_registro"));
				objRegistroUsuario.setNombres(rs.getString("nombres"));
				objRegistroUsuario.setApellidos(rs.getString("apellidos"));
				objRegistroUsuario.setCorreo(rs.getString("correo"));
				objRegistroUsuario.setSexo(rs.getString("sexo"));
				objRegistroUsuario.setUsuario(rs.getString("usuario"));
				objRegistroUsuario.setContrasena(rs.getString("contrasena"));
				objRegistroUsuario.setTipoUsuario(rs.getString("tipoUsuario"));

				lstResultado.add(objRegistroUsuario);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Envia correos.
	 * @author Aryam Ruiz
	 * @param Correo (obj)
	 */
	public Result SendMail(Correo correo) {
		Result result = new Result();
		Correo objCorreo = new Correo();
        Random random = new Random();
        
        String codigo = "";
        int matricula = 0;
		final String username = "contactoccsoft@gmail.com";
		final String password = "prueba123";
		final String to = correo.getTo();
        final String asunto = "Cambio de contrase�a";

        matricula=(int) (Math.random()*9999+1); 
        codigo = String.valueOf(matricula);
        
        String mensaje = "�Hola! [USUARIO]."+"\r\n"+"El siguiente c�digo te permite hacer el cambio de contrase�a, en caso de no ser tu quien lo solicita, contactanos. \r\n ==> [CODIGO] <==";
        mensaje = mensaje.replace("[USUARIO]", to);
        mensaje = mensaje.replace("[CODIGO]", codigo);
        
		updateClaveContrasena(correo.getTo(), codigo);
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", "587");
		props.setProperty("mail.smtp.auth", "true");
		
		 Session session = Session.getDefaultInstance(props,
	                new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(username, password);
	            }
	        });
		 
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(asunto);
			message.setText(mensaje);
			
			Transport.send(message);
         result.setError("false");
 
        } catch (Exception e) {
        	result.setError("true");
			result.setError_info(e.getMessage());
            throw new RuntimeException(e);
        }
        return result;
    }
	
	/**
	 * Actualiza el campo del codigo.
	 * @author Aryam Ruiz
	 * @param Codigo
	 */
	public final Result updateClaveContrasena(String correo, String codigo) {
		 Result result = new Result();
        try {
        	DBConnect db = new DBConnect();
			con = db.getConnection();
			con.setAutoCommit(false);

			pstm = con.prepareStatement(Statements.UPDATE_CODIGO_REGISTRO);
			pstm.setString(1, codigo);
			pstm.setString(2, correo);
			pstm.executeUpdate();
			
			con.commit();
			con.setAutoCommit(true);
			result.setError("false");
        } catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
			try {
				con.rollback();
				con.setAutoCommit(true);
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	 }
	 
	/**
	 * Obtiene los registros de RegistroUsuarios por clave de usuario.
	 * @author Aryam Ruiz
	 * @param username
	 */
	public Result getRegistroUsuariosPorClave(String clave) {
		Result result = new Result();
		List<RegistroUsuarios> lstResultado = new ArrayList<RegistroUsuarios>();
		RegistroUsuarios objRegistroUsuario = new RegistroUsuarios();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_TIPO_USUARIOS_POR_CLAVE);
			pstm.setString(1, clave);
			rs = pstm.executeQuery();

			while (rs.next()) {
				objRegistroUsuario = new RegistroUsuarios();
				objRegistroUsuario.setId_registro(rs.getInt("id_registro"));
				objRegistroUsuario.setNombres(rs.getString("nombres"));
				objRegistroUsuario.setApellidos(rs.getString("apellidos"));
				objRegistroUsuario.setCorreo(rs.getString("correo"));
				objRegistroUsuario.setUsuario(rs.getString("usuario"));
				objRegistroUsuario.setContrasena(rs.getString("contrasena"));
				objRegistroUsuario.setSexo(rs.getString("sexo"));
				objRegistroUsuario.setTipoUsuario(rs.getString("tipoUsuario"));
				objRegistroUsuario.setClave(rs.getString("clave"));

				lstResultado.add(objRegistroUsuario);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Actualiza el campo de la contrase�a
	 * @author Aryam Ruiz
	 * @param Codigo
	 */
	public final Result updateContrasena(RegistroUsuarios registroUsuarios) {
		 Result result = new Result();
        try {
        	DBConnect db = new DBConnect();
			con = db.getConnection();
			con.setAutoCommit(false);

			pstm = con.prepareStatement(Statements.UPDATE_CONTRASENA);
			pstm.setString(1, registroUsuarios.getContrasena());
			pstm.setString(2, registroUsuarios.getClave());
			pstm.executeUpdate();
			
			con.commit();
			con.setAutoCommit(true);
			result.setError("false");
        } catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
			try {
				con.rollback();
				con.setAutoCommit(true);
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	 }
	
	/**
	 * Envia correos soporte.
	 * @author Aryam Ruiz
	 * @param Correo (obj)
	 */
	
	/*public Result SendMailSoporte(CorreoSoporte correosoporte) {
		Result result = new Result();
		CorreoSoporte objCorreo = new CorreoSoporte();


		final String username = correosoporte.getCorreo();
		final String to = "contactoccsoft@gmail.com";
        final String asunto = correosoporte.getAsunto();
        String mensaje = correosoporte.getMensaje();
        String celular = correosoporte.getCelular();
        String nombre = correosoporte.getNombre();
        String host = "localhost";
        
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.starttls.enable", "true");

		
		props.put("mail.smtp.auth", "false");
		
		 Session session = Session.getDefaultInstance(props);
		 
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(asunto);
			message.setText(mensaje);
			message.setHeader(nombre, celular);
			
			Transport.send(message);
         result.setError("false");
 
        } catch (Exception e) {
        	result.setError("true");
			result.setError_info(e.getMessage());
            throw new RuntimeException(e);
        }
        return result;
    }*/
	/**
	 * Actualiza el campo del perfil
	 * @author Aryam Ruiz
	 * @param Codigo
	 */
	public final Result updatePerfil(RegistroUsuarios registroUsuarios) {
		 Result result = new Result();
        try {
        	DBConnect db = new DBConnect();
			con = db.getConnection();
			con.setAutoCommit(false);

			pstm = con.prepareStatement(Statements.UPDATE_PERFIL);
			pstm.setString(1, registroUsuarios.getUsuario());
			pstm.setString(2, registroUsuarios.getContrasena());
			pstm.setInt(3, registroUsuarios.getId_registro());
			pstm.executeUpdate();
			
			con.commit();
			con.setAutoCommit(true);
			result.setError("false");
        } catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
			try {
				con.rollback();
				con.setAutoCommit(true);
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	 }
	

}
