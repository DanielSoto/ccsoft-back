package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import db.DBConnect;
import db.Statements;
import entity.Reportes;
import entity.Result;

public class reportesDAO {
	// /*Se utiliza como loggeador de problemas para detectar fallos en ejecuciones de codigo*/
	private static Logger LOGGER = Logger.getLogger(relcatDAO.class);
	Connection con;
	Statement stm;
	ResultSet rs;
	PreparedStatement pstm;
	Integer metodo = 0;

	private void closeObj(Connection con, Statement stm, ResultSet rs, PreparedStatement pstm) {
		try {
			if (null != rs) {
				rs.close();
			}
			if (null != stm) {
				stm.close();
			}
			if (null != con) {
				con.close();
			}
			if (null != pstm) {
				pstm.close();
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage() + " - " + e.getErrorCode());
		}
	}

	/**
	 * Obtiene los registros de Reportes
	 * @author Aryam Ruiz
	 * @param
	 */
	public Result getReportes() {
		Result result = new Result();
		List<Reportes> lstResultado = new ArrayList<Reportes>();
		Reportes objReportes = new Reportes();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_REPORTES);
			rs = pstm.executeQuery();

			while (rs.next()) {
				objReportes = new Reportes();
				objReportes.setId_reportes(rs.getInt("id_reportes"));
				objReportes.setFolio(rs.getInt("folio"));
				objReportes.setNombre(rs.getString("nombre"));
				objReportes.setTelefono(rs.getString("telefono"));
				objReportes.setTelefonoAlt(rs.getString("telefonoAlt"));
				objReportes.setNombreDenunciado(rs.getString("nombreDenunciado"));
				objReportes.setCalle(rs.getString("calle"));
				objReportes.setEntreCalles(rs.getString("entreCalles"));
				objReportes.setColonia(rs.getString("colonia"));
				objReportes.setCp(rs.getInt("cp"));
				objReportes.setStatus(rs.getString("estatus"));
				objReportes.setDescripcion(rs.getString("descripcion"));
				objReportes.setCorreo(rs.getString("correo"));
				objReportes.setImgUrl(rs.getString("imgUrl"));
				objReportes.setFecha(rs.getString("fecha"));
				lstResultado.add(objReportes);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Inserta los datos de los reportes
	 * @author Aryam Ruiz
	 */
	public final Result insertReportes(Reportes reportes) {
		Result result = new Result();
		List<Reportes> lstResultado = new ArrayList<Reportes>();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();
			con.setAutoCommit(false);

			pstm = con.prepareStatement(Statements.INSERT_REPORTES);

			pstm.setInt(1, reportes.getFolio());
			pstm.setString(2, reportes.getNombre());
			pstm.setString(3, reportes.getTelefono());
			pstm.setString(4, reportes.getTelefonoAlt());
			pstm.setString(5, reportes.getNombreDenunciado());
			pstm.setString(6, reportes.getCalle());
			pstm.setString(7, reportes.getEntreCalles());
			pstm.setString(8, reportes.getColonia());
			pstm.setInt(9, reportes.getCp());
			pstm.setString(10, reportes.getStatus());
			pstm.setString(11, reportes.getDescripcion());
			pstm.setString(12, reportes.getCorreo());
			pstm.setString(13, reportes.getImgUrl());
			pstm.setString(14, reportes.getFecha());
			pstm.executeUpdate();

			con.commit();
			con.setAutoCommit(true);

			result.setSuccess(true);
			result.setError("false");
			result.setData(lstResultado);

		} catch (Exception e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e.getMessage());
			result.setError(e.getMessage());
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Actualiza los datos de los reportes
	 * @author Arturo Acu�a
	 */
	public final Result updateReportesEstatus(String estatus, Integer id) {
		Result result = new Result();
		List<Reportes> lstResultado = new ArrayList<Reportes>();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();
			con.setAutoCommit(false);

			pstm = con.prepareStatement(Statements.UPDATE_REPORTES_ESTATUS);

			pstm.setString(1, estatus);
			pstm.setInt(2, id);
			pstm.executeUpdate();

			con.commit();
			con.setAutoCommit(true);

			result.setSuccess(true);
			result.setError("false");
			result.setData(lstResultado);

		} catch (Exception e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e.getMessage());
			result.setError(e.getMessage());
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Obtiene los registros de Reportes para "Mis denuncias"
	 * @author Arturo Acu�a
	 * @param
	 */
	public Result getReportesDenunciasAdmin() {
		Result result = new Result();
		List<Reportes> lstResultado = new ArrayList<Reportes>();
		Reportes objReportes = new Reportes();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_REPORTES_DENUNCIAS_ADMIN);
			rs = pstm.executeQuery();
			while (rs.next()) {
				objReportes = new Reportes();
				objReportes.setId_reportes(rs.getInt("id_reportes"));
				objReportes.setNombre(rs.getString("nombre"));
				objReportes.setStatus(rs.getString("estatus"));
				objReportes.setDescripcion(rs.getString("descripcion"));
				objReportes.setCategoria(rs.getString("categoria"));
				objReportes.setFecha(rs.getString("fecha"));

				lstResultado.add(objReportes);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	
	/**
	 * Obtiene los registros de Reportes para "Mis denuncias"
	 * @author Arturo Acu�a
	 * @param
	 */
	public Result getReportesDenuncias(String usuario) {
		Result result = new Result();
		List<Reportes> lstResultado = new ArrayList<Reportes>();
		Reportes objReportes = new Reportes();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_REPORTES_DENUNCIAS);
			pstm.setString(1, usuario);
			rs = pstm.executeQuery();
			System.out.println(usuario);
			while (rs.next()) {
				objReportes = new Reportes();
				objReportes.setId_reportes(rs.getInt("id_reportes"));
				objReportes.setNombre(rs.getString("nombre"));
				objReportes.setStatus(rs.getString("estatus"));
				objReportes.setDescripcion(rs.getString("descripcion"));
				objReportes.setCategoria(rs.getString("categoria"));
				objReportes.setFecha(rs.getString("fecha"));

				lstResultado.add(objReportes);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
	/**
	 * Obtiene el ultimo ID de reporte insertado
	 * @author Arturo Acu�a
	 * @param
	 */
	public Result getReporteLastID() {
		Result result = new Result();
		List<Reportes> lstResultado = new ArrayList<Reportes>();
		Reportes objReportes = new Reportes();

		try {
			DBConnect db = new DBConnect();
			con = db.getConnection();

			pstm = con.prepareStatement(Statements.GET_REPORTE_LAST_ID);
			rs = pstm.executeQuery();

			while (rs.next()) {
				objReportes = new Reportes();
				objReportes.setId_reportes(rs.getInt("id_reportes"));
				lstResultado.add(objReportes);
			}

			result.setError("false");
			result.setSuccess(true);
			result.setData(lstResultado);
		} catch (Exception e1) {
			LOGGER.error("[EXCEPTION] Exception [MSG] " + e1.getMessage());
			result.setError("true");
			result.setError_info(e1.getMessage());
			e1.printStackTrace();
		} finally {
			closeObj(con, stm, rs, pstm);
		}
		return result;
	}
}
