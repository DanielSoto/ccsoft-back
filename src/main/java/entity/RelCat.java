package entity;
/**
 * @author aryam
 *
 */
public class RelCat {
	
	private Integer idCat;
	private Integer idReporte;
	private String nombre;
	
	
	public Integer getIdCat() {
		return idCat;
	}
	public void setIdCat(Integer idCat) {
		this.idCat = idCat;
	}
	public Integer getIdReporte() {
		return idReporte;
	}
	public void setIdReporte(Integer idReporte) {
		this.idReporte = idReporte;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String Nombre){
		this.nombre = Nombre;
	}

	
	@Override
	public String toString() {
		return "RelCat [idCat=" + idCat + ", idReporte=" + idReporte + "]";
	}

}
