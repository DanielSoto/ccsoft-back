package entity;

public class Correo {
	
	private String mensaje;
	private String to;
	private String subject;
	
	
	
	public String getMensaje() {
		return mensaje;
	}



	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}



	public String getTo() {
		return to;
	}



	public void setTo(String to) {
		this.to = to;
	}



	public String getSubject() {
		return subject;
	}



	public void setSubject(String subject) {
		this.subject = subject;
	}



	@Override
	public String toString() {
		return "Correo [mensaje=" + mensaje + ", to=" + to + ", subject=" + subject+"]";
	}


}
