package entity;
/**
 * @author aryam
 *
 */
public class Categorias {
	private Integer id_categorias;
	private String nombre;
	
	public Integer getId() {
		return id_categorias;
	}
	public void setId(Integer id_categorias) {
		this.id_categorias = id_categorias;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return "Categorias [id_categorias=" + id_categorias + ", nombre=" + nombre + "]";
	}
	
}
