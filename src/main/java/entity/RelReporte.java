package entity;
/**
 * @author aryam
 *
 */
public class RelReporte {
	
	private Integer idReporte;
	private Integer idUsuario;
	private String usuario;
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Integer getIdReporte() {
		return idReporte;
	}
	public void setIdReporte(Integer idReporte) {
		this.idReporte = idReporte;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public String toString() {
		return "RelReporte [idReporte=" + idReporte + ", idUsuario=" + idUsuario + "]";
	}

}
