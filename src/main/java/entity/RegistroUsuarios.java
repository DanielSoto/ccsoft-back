package entity;
/**
 * @author aryam
 *
 */
public class RegistroUsuarios {
	
	private Integer id_registro;
	private String nombres;
	private String apellidos;
	private String correo;
	private String sexo;
	private String usuario;
	private String contrasena;
	private String tipoUsuario;
	private Integer telefono;
	private String clave;
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public Integer getTelefono() {
		return telefono;
	}
	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}
	public Integer getId_registro() {
		return id_registro;
	}
	public void setId_registro(Integer id_registro) {
		this.id_registro = id_registro;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	@Override
	public String toString() {
		return "RegistroUsuarios [id_registro=" + id_registro + ", nombres=" + nombres + ", apellidos=" + apellidos + ", correo=" + correo + ", sexo=" + sexo 
				+  ", usuario="+ usuario + ", constrasena="+ contrasena + ", tipoUsuario="+ tipoUsuario +"]";
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	
}
