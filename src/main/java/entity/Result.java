/*
 * 
 */
package entity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class Resultado.
 */
@XmlRootElement(name = "Result")
@XmlAccessorType (XmlAccessType.FIELD)
/**
 * The Class Resultado.
 */

public class Result {
	private String error_info;
	/** The mensaje error. */
	private String error;
	public String getError_info() {
		return error_info;
	}

	public void setError_info(String error_info) {
		this.error_info = error_info;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	/** The success. */
	private boolean success;

	/** The lst resultado. */
	@XmlElement(name="data")
	private List<?> data;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public List<?> getAdditional_data() {
		return additional_data;
	}

	public void setAdditional_data(List<?> additional_data) {
		this.additional_data = additional_data;
	}
	
	@XmlElement(name="additional_data")
	private List<?> additional_data;
	

}
