package entity;
/**
 * @author aryam
 *
 */
public class Reportes {
	
	private Integer id_reportes;
	private Integer folio;
	private String nombre;
	private String telefono;
	private String telefonoAlt;
	private String nombreDenunciado;
	private String calle;
	private String entreCalles;
	private String colonia;
	private Integer cp;
	private String status;
	private String descripcion;
	private String correo;
	private String imgUrl;
	private String fecha;
	private String categoria;
	private String usuario;
	
	public Integer getId_reportes() {
		return id_reportes;
	}
	public void setId_reportes(Integer id_reportes) {
		this.id_reportes = id_reportes;
	}
	public Integer getFolio() {
		return folio;
	}
	public void setFolio(Integer folio) {
		this.folio = folio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTelefonoAlt() {
		return telefonoAlt;
	}
	public void setTelefonoAlt(String telefonoAlt) {
		this.telefonoAlt = telefonoAlt;
	}
	public String getNombreDenunciado() {
		return nombreDenunciado;
	}
	public void setNombreDenunciado(String nombreDenunciado) {
		this.nombreDenunciado = nombreDenunciado;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getEntreCalles() {
		return entreCalles;
	}
	public void setEntreCalles(String entreCalles) {
		this.entreCalles = entreCalles;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public Integer getCp() {
		return cp;
	}
	public void setCp(Integer cp) {
		this.cp = cp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public String toString() {
		return "RegistroUsuarios [id_reportes=" + id_reportes + ", folio=" + folio + ", nombre=" + nombre + ", telefono=" + telefono + ", telefonoAlt=" + telefonoAlt 
				+  ", nombreDenunciado="+ nombreDenunciado + ", calle="+ calle + ", entreCalles="+ entreCalles + ", colonia="+ colonia + ", cp="+ cp 
				+  ", status="+ status +  ", descripcion="+ descripcion + ", correo="+ correo + ", imgUrl="+ imgUrl + ", fecha="+fecha+"]";
	}

}
