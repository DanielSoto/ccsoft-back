package entity;
/**
 * 
 * @author artur
 *
 */
public class Dependencia {
	private Integer id_dependencia;
	private String nombre;
	private String municipio;
	private String calle;
	private String entreCalles;
	private String responsable;
	private String correo;
	private String telefono;
	
	public Integer getId_dependencia() {
		return id_dependencia;
	}
	public void setId_dependencia(Integer id_dependencia) {
		this.id_dependencia = id_dependencia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	public String getEntre_Calles() {
		return entreCalles;
	}
	public void setEntre_Calles(String entreCalles) {
		this.entreCalles = entreCalles;
	}
	
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	@Override
	public String toString() {
		return "Dependencia [id_dependencia=" + id_dependencia + ", nombre=" + nombre + ", municipio=" + municipio + ", calle=" + calle + ", entreCalles=" + entreCalles 
				+  ", responsable="+ responsable + ", correo="+ correo + ", telefono="+ telefono +"]";
	}
}
